# enrichPeaks #

This is an R package to get the statistical significance of the overlap between two peak sets. 
Put in all the peak files (BED format) in a folder and invoke R to use the package.

Download the package and install it as 


```

R CMD INSTALL package.tar.gz
cd folder-where-you-have-peak-files/ # *.bed

```

In the R console,

```

library(enrichPeaks)
?enrichPeaks
```